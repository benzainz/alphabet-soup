const fs = require('fs'); // Import the 'fs' module for file system operations.

function readLines(input) {
  return fs.readFileSync(input, 'utf8').split('\n'); // Read the content of the input file and split it into an array of lines.
}

function searchWord(grid, word, row, col) {
  const numRows = grid.length; // Get the number of rows in the grid.
  const numCols = grid[0].length; // Get the number of columns in the grid.
  const wordLength = word.length; // Get the length of the word to search for.

  // Check horizontally forward
  if (col + wordLength <= numCols) {
    const candidate = grid[row].slice(col, col + wordLength).join(''); // Extract a potential word in the horizontal direction.
    if (candidate === word) {
      return [row, col, row, col + wordLength - 1]; // Return the coordinates if the word is found.
    }
  }

  // Check horizontally backward
  if (col - wordLength + 1 >= 0) {
    const candidate = grid[row].slice(col - wordLength + 1, col + 1).reverse().join(''); // Extract a potential word in the reverse horizontal direction.
    if (candidate === word) {
      return [row, col, row, col - wordLength + 1]; // Return the coordinates if the word is found.
    }
  }

  return [-1, -1, -1, -1]; // Return default coordinates if the word is not found.
}

function searchWordDiagonalForward(grid, word, row, col) {
  const numRows = grid.length; // Get the number of rows in the grid.
  const numCols = grid[0].length; // Get the number of columns in the grid.
  const wordLength = word.length; // Get the length of the word to search for.

  if (row + wordLength <= numRows && col + wordLength <= numCols) {
    const candidate = [];
    for (let i = 0; i < wordLength; i++) {
      candidate.push(grid[row + i][col + i]); // Extract a potential word in the diagonal forward direction.
    }
    if (candidate.join('') === word) {
      return [row, col, row + wordLength - 1, col + wordLength - 1]; // Return the coordinates if the word is found.
    }
  }

  return [-1, -1, -1, -1]; // Return default coordinates if the word is not found.
}

function main() {
  const lines = readLines('input.txt'); // Read lines from the 'input.txt' file.
  const gridSize = lines[0].split('x'); // Split the first line to get the grid size.
  const numRows = parseInt(gridSize[0], 10); // Parse the number of rows as an integer.
  const numCols = parseInt(gridSize[1], 10); // Parse the number of columns as an integer.

  const grid = new Array(numRows); // Create an array to represent the grid.

  for (let r = 1; r <= numRows; r++) {
    const rowChars = lines[r].split(' '); // Split each row into individual characters.
    grid[r - 1] = rowChars.map((char) => char[0]); // Store the first character of each character array in the grid.
  }

  const wordsToFind = lines.slice(numRows + 1); // Extract the words to search for from the lines.

  for (const word of wordsToFind) {
    let found = false;
    let startRow = -1, startCol = -1, endRow = -1, endCol = -1;

    for (let r = 0; r < numRows; r++) {
      for (let c = 0; c < numCols; c++) {
        const result = searchWord(grid, word, r, c); // Search for the word in the grid.
        const diagonalResult = searchWordDiagonalForward(grid, word, r, c); // Search for the word diagonally.

        if (result[0] !== -1 || diagonalResult[0] !== -1) {
          startRow = result[0] !== -1 ? result[0] : diagonalResult[0]; // Get the starting row.
          startCol = result[1] !== -1 ? result[1] : diagonalResult[1]; // Get the starting column.
          endRow = result[2] !== -1 ? result[2] : diagonalResult[2]; // Get the ending row.
          endCol = result[3] !== -1 ? result[3] : diagonalResult[3]; // Get the ending column.

          console.log(`${word} ${startRow}:${startCol} ${endRow}:${endCol}`); // Output the found word and its coordinates.
          found = true;
          break;
        }
      }
      if (found) break;
    }
  }
}

main(); // Call the main function to start the program.

// //gitlab.com/enlighten-challenge/alphabet-soup

